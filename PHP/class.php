<?php

/*

cara membuat class di PHP

*/

class aClass {

    private $tes;

    /*

    function ini untuk menerima nilai default

    */

    public function __contruct($nama){
        $this->tes = $nama;
    }

    /*

    function ini untuk mengembalikan nilai default yang diinputkan user

    */

    public function great(){
        echo "hi ".$this->tes;
    }

}

/*

cara memanggil class di PHP

*/

$a = new aClass("astomo");
$a->great();

?>