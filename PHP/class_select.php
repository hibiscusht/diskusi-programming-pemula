<?php 

class MySelect {

private $conn;

public function __construct($host,$usr,$pass,$db){
    $this->conn = mysqli_connect($host,$usr,$pass,$db);
}

public function select_data($sql){
    $recordset = mysqli_query($this->conn,$sql);
    $data = Array();
    while($result = mysqli_fetch_assoc($recordset)){
        $data[] = $result;
    }
    return $data;
}


}

$myselect = new MySelect("localhost","root","","mydb");
$data = $myselect->select_data("SELECT * FROM rekening a JOIN atm b ON a.NoRek = b.NoATM");

//silahkan atur desain table HTML mulai dari sini

$k = "<table>";
$k .= "<tr><th>NoRek</th><th>Nama</th><th>TglLahir</th><th>Alamat</th><th>NoATM</th><th>TglBuat</th><th>TglAktif</th><th>Password</th></tr>";

foreach($data as $key => $val){
    $k .= "<tr>";
    foreach($val as $ku => $vu){
        $k .= "<td>$vu</td>";
    }
    $k .= "</tr>";
}

$k .= "</table>";

echo $k;

?>