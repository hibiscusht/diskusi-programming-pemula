CREATE DATABASE mydb;
USE mydb;
CREATE TABLE rekening (
NoRek TEXT NULL,
Nama TEXT NULL,
TglLahir DATE NULL,
Alamat TEXT NULL
);
CREATE TABLE atm (
NoATM TEXT NULL,
TglBuat DATE NULL,
TglAktif DATE NULL,
Password TEXT NULL
);
INSERT INTO rekening VALUES ('1234','Lorem Ipsum','2000-01-01','Neverland'),('1235','Ipsum Lorem','2002-01-01','Land Never');
INSERT INTO atm VALUES ('1234','2021-01-01','2021-01-01','tes123'),('1235','2021-01-15','2021-01-15','log234');