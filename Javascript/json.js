/*

function untuk mengubah string JSON menjadi object js

*/

function toJSON(json){
    let res = JSON.parse(json);
    return res;
}

let j = '{"rows":"[{"nama":"astomo","umur":"38"},{"nama":"nova","umur":"23"}]}';

let i = toJSON(j);
console.log(i);